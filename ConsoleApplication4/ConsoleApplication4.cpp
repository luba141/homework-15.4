#include <iostream>

void printNumbers(bool isEven, int N)
{
    for (int i = 0; i < N; ++i)
    {
        if (isEven && (i % 2 == 0))
        {
            std::cout << i << " ";
        }
        else if (!isEven && (i % 2 != 0))
        {
            std::cout << i << " ";
        }
    }
    std::cout << '\n';
}

int main()
{
    const int N = 15;
    for (int i = 0; i < N; ++i)
    {
        if (i % 2 == 0)
        {
            std::cout << i << " ";
        }
    }
    std::cout << '\n';
    printNumbers(true, 10);
    printNumbers(false, 10);
    return 0;
}